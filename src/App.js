import React, { useState } from "react";
import { connect } from "react-redux"
import {Route, NavLink, Switch} from "react-router-dom";
import { addTodo, clearCompleted } from "./actions";
import Todolist from "./components/TodoList";



const App = ({ todos, addTodo, clearCompleted }) => {
  const [newText, setInputText] = useState("");
  
  const handleAddTodo = (e) => {
    if (e.key === "Enter") {
      addTodo(newText)
      setInputText("");
    }
  };
  
 return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <input
          className="new-todo"
          placeholder="What needs to be done?"
          autofocus
          onChange={(event) => setInputText(event.target.value)}
          onKeyDown={handleAddTodo}
        />
      </header>
      <Switch>
        <Route
          exact
          path="/"
          render={(props) => (
            <Todolist
              {...props}
              todos={todos}
              
            />
          )}
        />
        <Route
          exact
          path="/active"
          render={(props) => (
            <Todolist
              {...props}
              todos={todos.filter((todo) => todo.completed === false)}
              
            />
          )}
        />
        <Route
          exact
          path="/completed"
          render={(props) => (
            <Todolist
              {...props}
              todos={todos.filter((todo) => todo.completed === true)}
              
            />
          )}
        />
      </Switch>
      <footer className="footer">
        <span className="todo-count">
      <strong>{todos.filter((todo)=>!todo.completed).length}</strong>item(s) left
        </span>
        <ul className="filters">
          <li>
            <NavLink to="/">all</NavLink>
          </li>
          <li>
            <NavLink to="/active">active</NavLink>
          </li>
          <li>
            <NavLink to="/completed">completed</NavLink>
          </li>
        </ul>
        <button onClick={(e) => clearCompleted()} className="clear-completed">
          Clear completed
        </button>
      </footer>
    </section>
  );
};

const mapStateToProps = (state) => ({
todos: state.todos,
});

const mapDispatchToProps = (dispatch) => ({
  addTodo: (text) => dispatch(addTodo(text)),
  clearCompleted: () => dispatch(clearCompleted()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);

