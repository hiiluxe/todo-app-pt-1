import React from "react"
import Todoitems from "./TodoItems"


const Todolist=({ todos })=> {
    return (
        <secton className="main">
            <ul className="todo-list">
                {todos.map((todo) => (
                    <Todoitems
                id={todo.id}
                title={todo.title}
                completed={todo.completed}
                todoId= {todo.id}/>
                ))}
            </ul>
        </secton>
    )
}
export default Todolist